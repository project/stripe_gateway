<?php

namespace Drupal\stripe_gateway\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\stripe_gateway\StripeServiceApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides form to cancel subscription.
 */
class StripeFormCancelSubscription extends FormBase {
  /**
   * The current account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The strive gateway service.
   *
   * @var \Drupal\stripe_gateway\StripeServiceApi
   */
  protected $stripeGatewayService;

  /**
   * Creates Btn Pay Subsciption object.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current account service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection service.
   * @param \Drupal\stripe_gateway\StripeServiceApi $stripeGatewayService
   *   The stripe gateway service.
   */
  public function __construct(
    AccountInterface $account,
    Connection $database,
    StripeServiceApi $stripeGatewayService
  ) {
    $this->account = $account;
    $this->database = $database;
    $this->stripeGatewayService = $stripeGatewayService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('database'),
      $container->get('stripe_gateway.api_service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'stripe_cancel_subscription_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $user = NULL, $id = NULL) {
    $user_id = $this->account->hasPermission('access user profiles') ? $user : $this->currentUser()->id();
    // Get subscription by id.
    $query = $this->database->select('ppss_sales', 's');
    $query->condition('id', $id);
    $query->condition('uid', $user_id);
    $query->condition('status', 1);
    $query->isNull('expire');
    $query->fields('s');
    $result = $query->execute()->fetchAssoc();
    if ($result) {
      $form['reason'] = [
        '#type' => 'select',
        '#title' => $this->t('Cancellation motive'),
        '#required' => TRUE,
        '#options' => [
          'too_expensive' => $this->t("It's too expensive"),
          'missing_features' => $this->t('Some features are missing'),
          'switched_service' => $this->t("I'm switching to a different service"),
          'unused' => $this->t("I don't use the service enough"),
          'customer_service' => $this->t('Customer service was less than expected'),
          'too_complex' => $this->t('Ease of use was less than expected'),
          'low_quality' => $this->t('Quality was less than expected'),
          'other' => $this->t('Other reason'),
        ],
      ];
      $form['id'] = [
        '#type' => 'hidden',
        '#required' => TRUE,
        '#default_value' => $id,
        '#description' => $this->t('ID sale'),
      ];

      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
      ];
    }
    else {
      $this->messenger()->addWarning($this->t('The subscription is already canceled'));
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $reason = $form_state->getValue('reason');
    $id = $form_state->getValue('id');
    // Call the service for cancellation.
    $cancel = $this->stripeGatewayService->cancelSubscription($id, $reason);
    $this->messenger()->addWarning($cancel);
    $form_state->setRedirect('user.page');
  }

}
