<?php

namespace Drupal\stripe_gateway\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\stripe_gateway\StripeServiceApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides an only one button form.
 */
class BtnPaySubscription extends FormBase {
  use StringTranslationTrait;

  /**
   * The current account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The strive gateway service.
   *
   * @var \Drupal\stripe_gateway\StripeServiceApi
   */
  protected $stripeGatewayService;

  /**
   * Creates Btn Pay Subsciption object.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current account service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   The logger service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request stack service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match service.
   * @param \Drupal\stripe_gateway\StripeServiceApi $stripeGatewayService
   *   The stripe gateway service.
   */
  public function __construct(
    AccountInterface $account,
    Connection $database,
    EntityTypeManagerInterface $entityTypeManager,
    LoggerChannelFactory $loggerFactory,
    RequestStack $request_stack,
    RouteMatchInterface $routeMatch,
    StripeServiceApi $stripeGatewayService
  ) {
    $this->account = $account;
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
    $this->loggerFactory = $loggerFactory;
    $this->requestStack = $request_stack;
    $this->routeMatch = $routeMatch;
    $this->stripeGatewayService = $stripeGatewayService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('request_stack'),
      $container->get('current_route_match'),
      $container->get('stripe_gateway.api_service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'stripe_button_pay_subscription';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Atempt to get the fully loaded node object of
    // the viewed page and settings.
    $node = $this->routeMatch->getParameter('node');
    $secretKey = $this->stripeGatewayService->getApiKey();

    // Only shows form if credentials are correctly configured and
    // content is a node.
    if (!(empty($secretKey) || (is_null($node)))) {
      // Check if the user has a subscription
      $query = $this->database->select('ppss_sales', 's');
      $query->condition('uid', $this->account->id());
      $query->condition('status', 1);
      $query->fields('s');
      $subscription = $query->execute()->fetchAssoc();
      
      $fieldPriceId = $this->config('stripe_gateway.settings')->get('field_price');
      $priceId = $node->get($fieldPriceId)->getString();

      // If subscription exists display plan update link.
      if ($subscription && $subscription['id_subscription']) {
        $details = json_decode($subscription['details']);

        $form['update'] = [
          '#type' => 'submit',
          '#value' => $node->id() == $details->node_id ? $this->t('Current plan') : $this->t('Change plan'),
          '#submit' => [[$this, 'submitUpdateForm']],
          '#disabled' => $node->id() == $details->node_id ? true : false
        ];
      } else {
        $form['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Buy Subscription Now'),
        ];
      }
    }
    else {
      // Nothing to display.
      $message = $this->t("Stripe gateway module don't has configured properly,
        please review your settings.");
      $this->loggerFactory->get('stripe gateway')->alert($message);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Atempt to get the fully loaded node object
    // of the viewed page and settings.
    $node = $this->routeMatch->getParameter('node');
    $config = $this->config('stripe_gateway.settings');
    $fieldPriceId = $config->get('field_price');
    $fieldRole = $config->get('field_role');
    $newRole = strlen($fieldRole) == 0 ? '' : $node->get($fieldRole)->getString();
    $taxRate = $config->get('tax_rate');
    // The price ID passed from the front end.
    $priceId = $node->get($fieldPriceId)->getString();

    // Define url.
    $request = $this->requestStack->getCurrentRequest();
    $baseUrl = $request->getSchemeAndHttpHost();
    // Stripe redirects to this page after the customer successfully
    // completes the checkout.
    $success = Url::fromRoute('stripe_gateway.payment_success', [], ['absolute' => TRUE])->toString();
    // Stripe redirects to this page when the customer clicks the back button
    // in Checkout.
    $cancel = $baseUrl . $this->config('ppss.settings')->get('error_url');

    $uid = $this->account->id();
    $user = $this->entityTypeManager->getStorage('user')->load($uid);
    $email = $user->get('mail')->value;

    // Get stripe client.
    $stripe = $this->stripeGatewayService->getStripeClient();

    /*
     * A Checkout Session controls what your customer sees in the
     * Stripe-hosted payment page.
     * Specify URLs for success and cancel pages—make sure they’re publicly
     * accessible so Stripe can redirect customers to them.
     * Use subscription mode to set up a subscription.
     * Checkout also has payment and setup modes.
     * Pass in the predefined price ID retrieved above.
     */

    $params = [
      'success_url' => $success . '?session_id={CHECKOUT_SESSION_ID}&roleid=' . $newRole,
      'cancel_url' => $cancel,
      'mode' => 'subscription',
      'payment_method_types' => ['card'],
      'line_items' => [
        [
          'price' => $priceId,
          'quantity' => 1,
          'tax_rates' => [$taxRate],
        ],
      ],
      // Save the product id.
      'metadata' => [
        'node_id' => $node->id(),
        'node_title' => $node->getTitle(),
      ],
    ];
    if ($email) {
      // Search for customers you’ve previously created.
      $customer = $stripe->customers->search([
        'query' => 'email:\'' . $email . '\'',
        'limit' => 1,
      ]);
      // Validate if customer exist.
      if ($customer->data) {
        // Set customer id to params.
        $params['customer'] = $customer->data[0]['id'];
      }
      else {
        // Set customer email to params.
        $params['customer_email'] = $email;
      }
    }
    // Creates a Session object.
    $session = $stripe->checkout->sessions->create($params);

    // Redirect to the URL returned on the Checkout Session.
    header('Location: ' . $session->url);
    exit();
  }

  public function submitUpdateForm(array &$form, FormStateInterface $form_state) {
    $node = $this->routeMatch->getParameter('node');
    $url = Url::fromRoute('stripe_gateway.confirm_subscription', ['nid' => $node->id()]);
    $form_state->setRedirectUrl($url);
  
  }

}
