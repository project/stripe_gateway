<?php

namespace Drupal\stripe_gateway\Plugin\QueueWorker;

use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\stripe_gateway\StripeServiceApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process a queue of webhook notification payload data.
 *
 * @QueueWorker(
 *   id = "stripe_webhook_processor",
 *   title = @Translation("Stripe Webhook notification processor"),
 *   cron = {"time" = 30}
 * )
 */
class StripeWebhookQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;


  /**
   * StripeServiceApi service.
   *
   * @var \Drupal\stripe_gateway\StripeServiceApi
   */
  protected $apiService;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactory $loggerFactory, StripeServiceApi $apiService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->loggerFactory = $loggerFactory;
    $this->apiService = $apiService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
    $configuration,
    $plugin_id,
    $plugin_definition,
    $container->get('logger.factory'),
    $container->get('stripe_gateway.api_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($payload) {
    // Only process the payload if it contains data.
    if (!empty($payload)) {

      // Decode the JSON payload to a PHP object.
      $entity_data = json_decode($payload);

      switch ($entity_data->type) {
        case 'customer.subscription.deleted':
          // A billing subscription was cancelled.
          $this->apiService->receiveWebhookCancel($entity_data);
          break;

        case 'invoice.payment_succeeded':
          // A payment completed.
          $this->apiService->paymentCompleted($entity_data);
          break;

        case 'customer.subscription.updated':
          // A customer subscription updated
          // $this->apiService->subscriptionUpdated($entity_data);
          break;

        default:
          $this->loggerFactory->get('stripe gateway')->info('Received unknown event type ' . $entity_data->type);
      }
    }
    else {
      $this->loggerFactory->get('stripe gateway')->error('Nothing data to process');
    }
  }

}
