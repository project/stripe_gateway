<?php

namespace Drupal\stripe_gateway\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides the Stripe pay subscription block.
 *
 * @Block(
 *   id = "stripe_btn_pay_subscription",
 *   admin_label = @Translation("Stripe Subscription Payment Button")
 * )
 */
class PaySubscriptionBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Creates Btn Pay Subsciption object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $configFactory,
    FormBuilderInterface $formBuilder,
    RouteMatchInterface $routeMatch,
    ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
    $this->formBuilder = $formBuilder;
    $this->routeMatch = $routeMatch;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('form_builder'),
      $container->get('current_route_match'),

    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = $this->formBuilder->getForm('Drupal\stripe_gateway\Form\BtnPaySubscription');

    // Takes the block title and prints inside the payment button like
    // call to action text.
    $form['submit']['#value'] = $this->configuration['label'];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account) {
    // If viewing a node, get the fully loaded node object.
    $node = $this->routeMatch->getParameter('node');

    // Only shows button in allowed node types.
    if (!(is_null($node))) {
      $nodeType = $node->getType();
      $allowedNodeTypes = $this->configFactory->get('ppss.settings')->get('content_types');
      $findedNodeType = array_search($node->getType(), $allowedNodeTypes);

      if ($nodeType == $findedNodeType) {
        return AccessResult::allowedIfHasPermission($account, 'view ppss button');
      }
    }

    return AccessResult::forbidden();

  }

}
