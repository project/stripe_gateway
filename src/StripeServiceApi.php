<?php

namespace Drupal\stripe_gateway;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ppss\PPSSServiceApi;
use Stripe\Exception\InvalidRequestException;
use Stripe\StripeClient;

/**
 * Service Api.
 */
class StripeServiceApi {
  use StringTranslationTrait;

  /**
   * Config Factory service object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * PPSS service.
   *
   * @var \Drupal\ppss\PPSSServiceApi
   */
  protected $ppssServiceApi;

  /**
   * Creates Stripe Service API object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config Factory service object.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   The logger service.
   * @param \Drupal\ppss\PPSSServiceApi $ppssServiceApi
   *   The ppss service api service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    Connection $database,
    EntityTypeManagerInterface $entityTypeManager,
    LoggerChannelFactory $loggerFactory,
    PPSSServiceApi $ppssServiceApi
  ) {
    $this->configFactory = $configFactory;
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
    $this->loggerFactory = $loggerFactory;
    $this->ppssServiceApi = $ppssServiceApi;
  }

  /**
   * Cancel subscription using the Stripe API server.
   *
   * @param int $id
   *   ID ppss_sale.
   * @param string $reason
   *   Cancellation reason.
   *
   * @return string
   *   cancellation status message
   */
  public function cancelSubscription($id, $reason) {
    // Get active subscription.
    $query = $this->database->select('ppss_sales', 's');
    $query->condition('id', $id);
    $query->condition('status', 1);
    $query->isNull('expire');
    $query->fields('s');
    $result = $query->execute()->fetchAssoc();
    if ($result) {
      $data = [];
      // Reason for cancellation.
      $data['reason'] = $reason;
      try {
        $stripe = $this->getStripeClient();
        $stripe->subscriptions->cancel(
          $result['id_subscription'],
          [
            'cancellation_details' => [
              'feedback' => $reason,
            ],
          ]
        );
        return $this->t('Cancellation subscription will be applied during the day.');

      }
      catch (InvalidRequestException $e) {
        return 'An error has occurred: ' . $e->getError()->message;
      }
    }
    else {
      return $this->t('Subscription is not active.');
    }
  }

  /**
   * Process queue of webhook notification event customer.subscription.deleted.
   *
   * @param \stdClass $entity_data
   *   Unique identifier for the object.
   * 
   * @return bool
   *   Returns true if the webhook was processed.
   */
  public function receiveWebhookCancel($entity_data) {
    // Get all payments.
    $query = $this->database->select('ppss_sales', 's');
    $query->join('ppss_sales_details', 'sd', 's.id = sd.sid');
    $query->condition('id_subscription', $entity_data->data->object->id);
    $query->fields('s', ['id', 'uid', 'status', 'id_role', 'id_subscription']);
    $query->fields('sd', ['id', 'created']);
    $query->orderBy('created', 'DESC');
    $results = $query->execute()->fetchAll();
    if (!empty($results)) {
      try {
        // Get the last payment.
        $subscription = $results[0];
        // Get subscription user.
        $user = $this->entityTypeManager->getStorage('user')
          ->load($subscription->uid);
        // Subscription expiration date.
        $expire = $entity_data->data->object->current_period_end;
        // Update ppss_sales table.
        $this->database->update('ppss_sales')->fields([
          'expire' => $expire,
        ])->condition('id_subscription', $entity_data->data->object->id, '=')
        ->execute();
        
        // Send email.
        $this->ppssServiceApi->sendEmail($user->getEmail(),
                'mail_scheduled_cancellation_subject', $expire);
        // Save log message.
        $this->loggerFactory->get('stripe gateway')->info($this->t('User
          @user subscription @id_subscription will be canceled on @expire', [
            '@id_subscription' => $subscription->id_subscription,
            '@user' => $subscription->uid,
            '@expire' => date('d/m/Y', $expire),
          ]));
        // Unpublish all ads setting a new date in the future.
        $this->ppssServiceApi->unpublishNodes($subscription->uid, $expire);
        return true;
      }
      catch (\Exception $e) {
        $this->loggerFactory->get('stripe gateway')->error($e->getMessage());
        return false;
      }
    } else {
      return false;
    }
  }

  /**
   * Save payment recurrent.
   *
   * @param \stdClass $entity_data
   *   Data receive from webhook notification.
   * 
   * @return bool
   *   Returns true if the webhook was processed.
   */
  public function paymentCompleted($entity_data) {
    try {
      $webhook_event_id = $entity_data->id;
      $subscriptionID = $entity_data->data->object->subscription;
      // Get data subscription from database.
      $query = $this->database->select('ppss_sales', 's');
      $query->condition('id_subscription', $subscriptionID);
      $query->fields('s', ['id', 'uid', 'frequency', 'status', 'details', 'id_subscription', 'id_role']);
      $results = $query->execute()->fetchAll();
      $subscription = $results[0];
      $details = json_decode($subscription->details);
      // validate that the webhook has not been registered before
      $existWebhook = $this->database->select('ppss_sales_details', 's')
        ->condition('event_id', $webhook_event_id)
        ->fields('s')->execute()->fetchAssoc();
      if ($existWebhook) {
        return false;
      }
      //created subscription webhook - first payment
      if ($entity_data->data->object->billing_reason == 'subscription_create') {
        $payment = $this->database->select('ppss_sales_details', 's')
          ->condition('sid', $subscription->id)
          ->condition('event_id', 0)
          ->fields('s')
          ->execute()->fetchAll();
          // update event_id from webhook.
          $this->database->update('ppss_sales_details')->fields([
            'event_id' => $webhook_event_id,
          ])->condition('id', $payment[0]->id)->execute();
      }
      // updated subscription webhook - recurring payment
      else {
        $stripe = $this->getStripeClient();
        // Get subscription details from stripe
        $getSub = $stripe->subscriptions->retrieve($subscriptionID, []);
        $newRole = $getSub->items->data[0]['price']['metadata']['rol'];
        $newNode = $getSub->items->data[0]['price']['metadata']['nid'];
        // Get product details
        $product = $stripe->products->retrieve($getSub->items->data[0]->plan->product, []);
        // Insert a new recurring payment.
        if ($entity_data->data->object->amount_paid > 0) {
          $query = $this->database->insert('ppss_sales_details');
          $query->fields(['sid', 'tax', 'price', 'total', 'created', 'event_id', 'description']);
          $query->values([
            $subscription->id,
            number_format($entity_data->data->object->tax / 100, 2, '.', ''),
            number_format($entity_data->data->object->subtotal_excluding_tax / 100, 2, '.', ''),
            number_format($entity_data->data->object->total / 100, 2, '.', ''),
            $entity_data->created,
            $webhook_event_id,
            $product->name,
          ]);
          $query->execute();
        }
        // Update plan
        // Validate if the plan was changed
        if ($details->node_id != $newNode) {
          // Get user
          $user = $this->entityTypeManager->getStorage('user')->load($subscription->uid);
          // Delete previous role
          $user->removeRole($subscription->id_role);
          // Assign the new role
          $user->addRole($newRole);
          $user->save();
          // Assign the new data
          $previousPlan = $details->description;
          $details->node_id = $newNode;
          $details->description = $product->name;
          
          // Update the table with the new plan
          $this->database->update('ppss_sales')->fields([
            'frequency' => $getSub->items->data[0]->plan->interval,
            'id_role' => $newRole,
            'details' => json_encode($details),
          ])->condition('id', $subscription->id, '=')->execute();

          $this->loggerFactory->get('stripe gateway')->info($this->t('User
           @usuario went from @previous_plan to the @new_plan.', [
            '@usuario' => $subscription->uid,
            '@previous_plan' => $previousPlan,
            '@new_plan' => $product->name,
          ]));
        }
      }
      $this->loggerFactory->get('stripe gateway')->info($this->t('Recurring
      payment of the subscription @sub was made for $ @amount', [
        '@sub' => $subscriptionID,
        '@amount' => number_format($entity_data->data->object->amount_paid / 100, 2, '.', '')
      ]));
      return true;
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('stripe gateway')->error($e->getMessage());
      return false;
    }
  }

  /**
   * Get a Stripe Client.
   *
   * @return \Stripe\StripeClient
   *   The StripeClient.
   */
  public function getStripeClient() {
    return new StripeClient($this->getApiKey());
  }

  /**
   * Get secret key.
   *
   * @return string
   *   Secret key.
   */
  public function getApiKey() {
    $config = $this->configFactory->get('stripe_gateway.settings');
    // See your keys here: https://dashboard.stripe.com/apikeys
    if ($config->get('sandbox_mode')) {
      return $config->get('secret_key_test');
    }
    else {
      return $config->get('secret_key_live');
    }
  }

}
