<?php

namespace Drupal\stripe_gateway\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Queue\QueueFactory;
use Drupal\stripe_gateway\StripeServiceApi;
use Stripe\Exception\SignatureVerificationException;
use Stripe\Stripe;
use Stripe\Webhook;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Defines a controller for managing webhook notifications.
 */
class StripeWebhookController extends ControllerBase {

  /**
   * Config Factory service object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The HTTP request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The strive gateway service.
   *
   * @var \Drupal\stripe_gateway\StripeServiceApi
   */
  protected $stripeGatewayService;

  /**
   * Constructs a PPSSWebhookController object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config Factory service object.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   The logger service.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HTTP request object.
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   The queue factory.
   * @param \Drupal\stripe_gateway\StripeServiceApi $stripeGatewayService
   *   The stripe gateway service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactory $loggerFactory,
    Request $request,
    QueueFactory $queue,
    StripeServiceApi $stripeGatewayService
  ) {
    $this->configFactory = $configFactory;
    $this->loggerFactory = $loggerFactory;
    $this->request = $request;
    $this->queueFactory = $queue;
    $this->stripeGatewayService = $stripeGatewayService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('config.factory'),
        $container->get('logger.factory'),
        $container->get('request_stack')->getCurrentRequest(),
        $container->get('queue'),
        $container->get('stripe_gateway.api_service'),
      );
  }

  /**
   * Listens for webhook notifications and queues them for processing.
   *
   *  @return \Symfony\Component\HttpFoundation\Response
   *   Webhook providers typically expect an HTTP 200 (OK) response.
   */
  public function listener() {
    // Prepare the response.
    $response = new Response();
    // Capture the contents of the notification (payload).
    $payload = $this->request->getContent();
    $service = false;

    try {
      // Decode the JSON payload to a PHP object.
      $entity_data = json_decode($payload);

      switch ($entity_data->type) {
        case 'customer.subscription.deleted':
          // A billing subscription was cancelled.
          $service = $this->stripeGatewayService->receiveWebhookCancel($entity_data);
          break;
        case 'invoice.payment_succeeded':
          // A payment completed.
          $service = $this->stripeGatewayService->paymentCompleted($entity_data);
          break;
        default:
          $this->loggerFactory->get('stripe gateway')->info('Received unknown event type ' . $entity_data->type);
      }
      if ($service) {
        // Respond with the success message.
        $response->setContent('Notification received');
        return $response;
      } else {
        throw new BadRequestHttpException('Error processing webhook');
      }
    }
    catch (\Exception $exception) {
      $this->loggerFactory->get('stripe gateway')->error('Error processing webhook');
      throw new BadRequestHttpException('Error processing webhook');
    }
  }

  /**
   * Checks access for incoming webhook notifications.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access() {
    $config = $this->configFactory->get('stripe_gateway.settings');
    $secretKey = $this->stripeGatewayService->getApiKey();

    Stripe::setApiKey($secretKey);
    // Replace this endpoint secret with your endpoint's unique secret
    // If you are using an endpoint defined with the API or dashboard,
    // look in your webhook settings
    // at https://dashboard.stripe.com/webhooks
    $endpointSecret = $config->get('secret_webhook');

    $payload = $this->request->getContent();
    $verifyAccess = 0;

    // Only verify the event if there is an endpoint secret defined.
    if ($endpointSecret) {
      $sigHeader = $this->request->headers->get('stripe-signature');
      try {
        $event = Webhook::constructEvent(
          $payload,
          $sigHeader,
          $endpointSecret
        );
        $verifyAccess = 1;
      }
      catch (\UnexpectedValueException $e) {
        // Invalid payload.
        $this->loggerFactory->get('stripe webhook')->error($this->t('Error parsing payload:') . ' ' . $e->getMessage());
      }
      catch (SignatureVerificationException $e) {
        // Invalid signature.
        $this->loggerFactory->get('stripe webhook')->error($this->t('Error verifying webhook signature:') . ' ' . $e->getMessage());
      }
    }
    // If they validation was successful, allow access to the route.
    return AccessResult::allowedIf($verifyAccess);
  }

}
