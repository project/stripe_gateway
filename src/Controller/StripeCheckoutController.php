<?php

namespace Drupal\stripe_gateway\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\stripe_gateway\StripeServiceApi;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines a controller for managing stripe checkout notifications.
 */
class StripeCheckoutController extends ControllerBase {
  use StringTranslationTrait;

  /**
   * The current account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Drupal\stripe_gateway\StripeServiceApi definition.
   *
   * @var \Drupal\stripe_gateway\StripeServiceApi
   */
  protected $stripeServiceApi;

  /**
   * Used to get the current return URL, plus the query parameters.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The datetime.time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $timeService;

  /**
   * UserSubscriptionsController constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current account service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   The logger service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match service.
   * @param \Drupal\stripe_gateway\StripeServiceApi $stripeServiceApi
   *   The stripe gateway service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The current request service.
   * @param \Drupal\Component\Datetime\TimeInterface $time_service
   *   The datetime service.
   */
  public function __construct(
    AccountInterface $account,
    Connection $database,
    EntityTypeManagerInterface $entityTypeManager,
    LoggerChannelFactory $loggerFactory,
    MessengerInterface $messenger,
    RouteMatchInterface $routeMatch,
    StripeServiceApi $stripeServiceApi,
    RequestStack $requestStack,
    TimeInterface $time_service
  ) {
    $this->account = $account;
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
    $this->loggerFactory = $loggerFactory;
    $this->messenger = $messenger;
    $this->routeMatch = $routeMatch;
    $this->stripeServiceApi = $stripeServiceApi;
    $this->currentRequest = $requestStack->getCurrentRequest();
    $this->timeService = $time_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('current_route_match'),
      $container->get('stripe_gateway.api_service'),
      $container->get('request_stack'),
      $container->get('datetime.time')
    );
  }

  /**
   * Function to process payment.
   * 
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *  Redirects user to new URL.
   */
  public function successCheckout() {
    // Get stripe client.
    $stripe = $this->stripeServiceApi->getStripeClient();
    // Retrieve the Checkout Session ID, newRole, plan from the URL parameter.
    $sessionId = $this->currentRequest->query->get('session_id');
    $newRole = $this->currentRequest->query->get('roleid');
    $isAnonymous = $this->account->isAnonymous();
    // Retrieve a Session.
    $session = $stripe->checkout->sessions->retrieve($sessionId);
    $email = $session['customer_details']->email;

    $subscriptionID = $session->subscription;
    if ($session->payment_status === 'paid') {
      // The payment is successful. You can handle any additional actions here.
      // validate purchase register in ppss_sales.
      $query = $this->database->select('ppss_sales', 's')->condition('id_subscription', $subscriptionID);
      $numRows = $query->countQuery()->execute()->fetchField();
      if ($numRows == 0) {
        // Retrieves the subscription with the given ID.
        $subscription = $stripe->subscriptions->retrieve(
          $subscriptionID,
          []
        );
        // Retrieve a product by id.
        $product = $stripe->products->retrieve(
          $subscription['plan']->product, []
        );
        // Save name product.
        $session['description'] = $product->name;
        // Save the id node to query the product.
        $session['node_id'] = $session['metadata']->node_id;
        $session['tax'] = $subscription->items->data[0]->tax_rates[0]->percentage;

        // Create or update user.
        if ($isAnonymous) {
          // It's an anonymous user. First will search about if returned email
          // by Stripe exist,
          // if not, trying to create an account with data returned.
          $ids = $this->entityTypeManager->getStorage('user')->getQuery()
            ->accessCheck(TRUE)
            ->condition('mail', $email)
            ->execute();

          // Find if email exist.
          if (!empty($ids)) {
            // This mail already exists.
            // Only will assign the role of the subscription.
            $uid = intval(current($ids));
            try {
              $user = $this->entityTypeManager->getStorage('user')->load($uid);
              $user->addRole($newRole);
              $user->save();
            }
            catch (\Exception $e) {
              $errorInfo = $this->t('Charge was made correctly but something was wrong when trying
                to assign the new subscription plan to your account. Please contact
                with the site administrator and explain this situation.');
              // Show error message to the user.
              $this->messenger->addError($errorInfo);
              $this->loggerFactory->get('stripe gateway')->error($errorInfo);
              $this->loggerFactory->get('stripe gateway')->error($e->getMessage());
            }
            $this->messenger->addMessage($this->t('Please login with your user account linked to this email:
              @email for begin use our services.', ['@email' => $email]));
          }
          else {
            // Creates a new user with the Stripe email.
            try {
              // Get te user name to register from the email.
              $temp = explode('@', $email);
              $userName = $temp[0];
              $user = User::create();
              $user->set('status', 1);
              $user->setEmail($email);
              $user->setUsername($userName);
              $user->addRole($newRole);
              $user->enforceIsNew();
              $user->save();

            }
            catch (\Exception $e) {
              $errorInfo = $this->t('Charge was made correctly but something was wrong when trying
                to create your account. Please contact with the site administrator
                and explain this situation.');
              // Show error message to the user.
              $this->messenger->addError($errorInfo);
              $this->loggerFactory->get('stripe gateway')->error($errorInfo);
              $this->loggerFactory->get('stripe gateway')->error($e->getMessage());
            }

            // Send confirmation email.
            $result = _user_mail_notify('register_no_approval_required', $user);

            if ($result) {
              $this->messenger->addMessage($this->t('Please review your email: @email to login details
                and begin use our services.', ['@email' => $email]));
            } else {
              $message = $this->t('There was a problem sending your email notification to @email.',
                ['@email' => $email]);
              $this->messenger->addError($message);
              $this->loggerFactory->get('stripe gateway')->error($message);
            }
            // Get the uid of the new user.
            $ids = $this->entityTypeManager->getStorage('user')->getQuery()
              ->accessCheck(TRUE)
              ->condition('mail', $email)
              ->execute();
          }

          $uid = intval(current($ids));

        }
        else {
          // Only will assign the role of the subscription
          // plan purchased to the current user.
          $uid = $this->account->id();
          try {
            $user = $this->entityTypeManager->getStorage('user')->load($uid);
            $user->addRole($newRole);
            $user->save();
            $this->messenger->addMessage($this->t('Your user account linked to this email: @email was successfully
              upgraded, please enjoy.', ['@email' => $email]));
          }
          catch (\Exception $e) {
            // Show error message to the user.
            $errorInfo = $this->t('Charge was made correctly but something was wrong when trying
              to assign the new subscription plan to your account. Please contact
              with the site administrator and explain this situation.');
            $this->messenger->addError($errorInfo);
            $this->loggerFactory->get('stripe_gateway')->error($errorInfo);
            $this->loggerFactory->get('stripe_gateway')->error($e->getMessage());
          }
        }

        // Save all transaction data in DB for future reference.
        try {
          // Initiate missing variables to save.
          $currentTime = $this->timeService->getRequestTime();

          // Save the values to the database
          // Start to build a query builder object $query.
          // Ref.: https://www.drupal.org/docs/drupal-apis/database-api/insert-queries
          $newSale = $this->database->insert('ppss_sales');
          // Specify the fields taht the query will insert to.
          $newSale->fields([
            'uid',
            'status',
            'mail',
            'platform',
            'frequency',
            'frequency_interval',
            'details',
            'created',
            'id_subscription',
            'id_role',
          ]);

          // Set the values of the fields we selected.
          // Note that then must be in the same order as
          // we defined them in the $query->fields([...]) above.
          $newSale->values([
            $uid,
            1,
            $email,
            'Stripe',
            $subscription['plan']->interval,
            $subscription['plan']->interval_count,
            str_replace('Stripe\Checkout\Session JSON:', '', $session),
            $currentTime,
            $subscriptionID,
            $newRole,
          ]);
          // Execute the query!
          $newSale->execute();

          // Get the ppss_sales data inserted above.
          $getSale = $this->database->select('ppss_sales', 's');
          $getSale->condition('id_subscription', $subscriptionID);
          $getSale->fields('s', ['id']);
          $subscription = $getSale->execute()->fetchAssoc();

          // Insert sales details.
          $newSaleDetail = $this->database->insert('ppss_sales_details');
          $newSaleDetail->fields(['sid', 'tax', 'price', 'total', 'created', 'event_id', 'description']);
          $newSaleDetail->values([
            $subscription['id'],
            number_format($session['total_details']->amount_tax / 100, 2, '.', ''),
            number_format(($session->amount_total - $session['total_details']->amount_tax) / 100, 2, '.', ''),
            number_format($session->amount_total / 100, 2, '.', ''),
            $currentTime,
            0,
            $product->name
          ]);
          $newSaleDetail->execute();

          $this->messenger->addMessage($this->t('Successful subscription purchase.'));
        }
        catch (\Exception $e) {
          // Show error message to the user.
          $errorInfo = $this->t('Unable to save payment to DB at this time due to database error.
            Please contact with the site administrator and explain this situation.');
          $this->messenger->addError($errorInfo);
          $this->loggerFactory->get('stripe gateway')->error($errorInfo);
          $this->loggerFactory->get('stripe gateway')->error($e->getMessage());
        }
      }
      return new RedirectResponse($this->config('ppss.settings')->get('success_url'));

    }
    else {
      // The payment is not yet completed or failed. Handle accordingly.
      $this->messenger->addMessage($this->t('Payment not completed. Please contact support.'));
      return new RedirectResponse($this->config('ppss.settings')->get('cancel_url'));
    }
  }

  /**
   * Purchase history by user.
   * 
   * @return array
   *  Return the user's purchase data.
   */
  public function purchaseStripe() {
    $user_id = $this->account->hasPermission('access user profiles') ?
      $this->routeMatch->getParameter('user') : $this->currentUser()->id();

    // Create table header.
    $header_table = [
      'name' => $this->t('Plan'),
      'platform' => $this->t('Payment type'),
      'date' => $this->t('Start date'),
      'status' => $this->t('Status'),
      'details' => $this->t('Details'),
      'billing' => '',
    ];

    // Select records from table ppss_sales.
    $query = $this->database->select('ppss_sales', 's');
    $query->condition('uid', $user_id);
    $query->fields('s', ['id', 'uid', 'mail', 'platform', 'details', 'created', 'status', 'id_subscription']);
    $results = $query->execute()->fetchAll();

    $rows = [];
    foreach ($results as $data) {
      $details = json_decode($data->details);
      $operations = Url::fromRoute('stripe_gateway.manage_subscription', ['customer' => $details->customer], []);
      $baseUrl = $this->currentRequest->getSchemeAndHttpHost();
      $url = Url::fromUri($baseUrl . '/user/' . $user_id . '/show_purchase/' . $data->id);

      // Print the data from table.
      $rows[] = [
        'name' => $details->description,
        'platform' => $data->platform,
        'date' => date('d/m/Y', $data->created),
        'status' => $data->status ? $this->t('Active') : $this->t('Inactive'),
        'details' => Link::fromTextAndUrl($this->t('Details'), $operations),
        'invoice' => $this->config('stripe_gateway.settings')->get('invoice') ?
        Link::fromTextAndUrl($this->t('Billing'), $url)->toString() : '',
      ];
    }
    // Display data in site.
    $form['table'] = [
      '#type' => 'table',
      '#header' => $header_table,
      '#rows' => $rows,
      '#empty' => $this->t('Without purchases'),
    ];
    return $form;
  }

  /**
   * Integrate the customer portal.
   *
   * @param string $customer
   *   Customer id
   */
  public function manageSubscription($customer) {
    try {
      // Get stripe client.
      $stripe = $this->stripeServiceApi->getStripeClient();
      // Creates a session of the customer portal.
      // https://stripe.com/docs/api/customer_portal/sessions/create
      $session = $stripe->billingPortal->sessions->create([
        'customer' => $customer,
        'return_url' => $this->currentRequest->getSchemeAndHttpHost() .
        $this->config('ppss.settings')->get('error_url'),
      ]);
      header('Location: ' . $session->url);
      exit();

    }
    catch (\Exception $exception) {
      return [
        '#markup' => $this->t('Something went wrong! @error', ['@error' => $exception->getMessage()]),
      ];
    }
  }

  /**
   * Confirm subscription update.
   *
   * @param int $nid
   *   node id
   * 
   * @return array
   *   Return the subscription change data.
   */
  public function confirmSubscription($nid) {
    $current_subscription = $this->database->select('ppss_sales', 's')
      ->condition('uid', $this->account->id())
      ->condition('status', 1)
      ->fields('s')
      ->execute()->fetchAssoc();
    $details = json_decode($current_subscription['details']);
    $currentPrice = $details->amount_total;

    // Get node
    $node = $this->entityTypeManager->getStorage('node')->load($nid);
    $fieldPriceId = $this->config('stripe_gateway.settings')->get('field_price');
    $newPrice = $node->get($fieldPriceId)->getString();

    try {
      // Get stripe client.
      $stripe = $this->stripeServiceApi->getStripeClient();
      // Get new plan data
      $newPlan = $stripe->prices->retrieve($newPrice, []);
      // Get subscription data
      $subscription = $stripe->subscriptions->retrieve($current_subscription['id_subscription']);
      // See what the next invoice would look like with a price switch
      // and proration set:
      $invoice = $stripe->invoices->upcoming([
        'customer' => $details->customer,
        'subscription' => $current_subscription['id_subscription'],
        'subscription_details' => [
          'items' => [
            [
              'id' => $subscription->items->data[0]->id,
              'price' => $newPrice, # Switch to new price
              'tax_rates' => [$this->config('stripe_gateway.settings')->get('tax_rate')]
            ],
          ],
          'proration_behavior' => $newPlan->unit_amount > $currentPrice  ? 'always_invoice' : 'none',
        ]
      ]);

      $interval = $newPlan['recurring']->interval;
      // Data to display in the template
      $data = [
        'subscription' => $current_subscription['id_subscription'],
        'itemID' => $subscription->items->data[0]->id,
        'priceID' => $newPrice,
        'node' => $nid,
        'plan' => $node->getTitle(),
        'price' => number_format($newPlan->unit_amount / 100, 2, '.', ''),
        'interval' => $interval,
        'proration' => $newPlan->unit_amount > $currentPrice ? 1 : 0,
        'period_start' => strtotime(date('Y-m-d', $invoice->period_start). ' + 1 ' . $interval),
        'total_adeudado' => number_format($invoice->total / 100, 2, '.', '')
      ];

      return [
        '#theme' => 'update-subscription',
        '#data' => $data,
        '#cache' => ['max-age' => 0],
      ];
    }
    catch (\Exception $exception) {
      return [
        '#markup' => $this->t('Something went wrong! @error',
          ['@error' => $exception->getMessage()]),
      ];
    }

  }

  /**
   * Update subscription.
   *
   * @param string $subscription
   *   Subscription id
   * @param string $itemID
   *   Subscription item id
   *  @param string $priceID
   *   New price id
   *  @param bool $proration
   *   True if apply proration
   * 
   * @return array
   *   Return the subscription change data
   */
  public function updateSubscription($subscription, $itemID, $priceID, $proration) {
    $stripe = $this->stripeServiceApi->getStripeClient();
    try {
      $data = $stripe->subscriptions->update($subscription, [
        'items' => [
          [
            'id' => $itemID,
            'price' => $priceID,
            'tax_rates' => [$this->config('stripe_gateway.settings')->get('tax_rate')],
          ],
        ],
        'proration_behavior' => $proration ? 'always_invoice' : 'none',
      ]);
      return [
        '#markup' => $this->t('The subscription has been updated.'),
      ];
    } catch (\Exception $exception) {
      return [
        '#markup' => $this->t('Something went wrong! @error',
          ['@error' => $exception->getMessage()]),
      ];
    }

  }
}
