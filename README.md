# Stripe Gateway

## Introduction

The main purpose of this module is Allow Drupal users
to subscribe to Stripe plans and have one or more Drupal roles.
Stripe allows to accept Visa, Mastercard, American Express,
Discover, JCB, Diners Club, China UnionPay and debit cards,
can also accept payments from mobile wallets and buy now,
pay later services. Currently is a work in progress.

## Requirements

This module requires the [Drupal module PPSS.](https://www.drupal.org/project/ppss)

## Installation

- Install as you would normally install a contributed Drupal module. See [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for more details
- Register to Stripe if you haven't yet

## Configuration

- **Configure Stripe**

  - **Create Account in Stripe**

    Go to [Stripe and register](https://dashboard.stripe.com/register)
  - **[Create products and prices](https://support.stripe.com/questions/how-to-create-products-and-prices)**
  
    Configure the following metadata for the price, these are mandatory because
    they are used for upgrade and downgrade subscriptions.

    rol: role that will be assigned to the user when the plan is purchased.

    nid: id of the product that has this price.

  - **[Configure a tax rate](https://stripe.com/docs/billing/taxes/tax-rates)**
  - **Configure webhooks**
    - Go to [webhooks](https://dashboard.stripe.com/webhooks)
    - Enter name and description
    - Enter Endpoint URL
    - Select events (invoice.payment_succeeded, customer.subscription.deleted)

- **Configure Drupal**

  In Drupal main menu go to:
  Configuration » Web services » Platform of Payments and Sales Simple

  - Verify if Stripe was selected like **payment gateway**, if not, select it.
  - Choose the **content types**

    On the list of content types, select the node types
    you need to be able to make purchases.

  Now select the **Stripe** tab.

  - Enter the **Sandbox mode**

    Stripe’s test mode allows you to test your integration
    without making actual charges or payments.
  - Enter the **Public key** (optional)

    Public key to authenticate client-side when are you ready
    for move to production environment.
  - Enter the **Secret key**

    Secret key to authenticate API requests. (Required)

    **You can find your secret and publishable keys on the API keys page in the Developers Dashboard.**

  Fields configuration:
  - Enter the **Price field name**

    Name of the field to store subscription price id.

  - Enter the **User role field name**

    Name of the field to store new user role assigned after purchased a plan.

  Webhook configuration:

  - Enter the **Secret Key**

    Webhooks endpoint secret key (whsec_...).

  - Click Save configuration.

  After saving the configuration:
    - Add/update your product entity with Price ID
      (example: price_1OJe7QJT0uvSCYSGP8IMxxx) and role ID.
    - Add the **Stripe Subscription Payment Button** block
      for the entities where you want to
      implement the stripe payment.
    - Enable **View PPSS Button** permission to display the block.

 **You can find Price ID on Stripe Dashboard products.**

## How it works

This module provides the Stripe Payment block.
We add this block to the node and entities where
we can add the price id for that product or entity.
The price id will be displayed as a button.
The button redirects customers to a Stripe-hosted payment
page to complete their purchase.
After the transaction successfully the role is assigned to the user.
The user can also upgrade and downgrade subscriptions.

## Maintainers

- Alejandro Gómez Lagunas - [agomezguru](https://www.drupal.org/u/agomezguru)
- Laura Ramos - [lauraramos](https://www.drupal.org/u/lauraramos)
